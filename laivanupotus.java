import java.util.*;
import java.util.Scanner;

public class laivanUpotus {

	Scanner input = new Scanner(System.in);
	public static final boolean DEBUG = false;

	public static void valiViiva() {
		System.out.println("__________________");
		System.out.println("");
	}

	public static void luoAlusta(String[][] taulu) {
		for (int r = 0; r < taulu.length; r++) {
			for (int c = 0; c < taulu[0].length; c++) {
				taulu[r][c] = "~";
			}
		}
	}

	public static void naytaTaulu(String[][] taulu) {
		valiViiva();
		for (int r = 0; r < taulu.length; r++) {
			if (DEBUG == true) {
				for (int c = 0; c < taulu[0].length; c++) {
					System.out.print(" " + taulu[r][c]);
				}
				System.out.println("");
			} else {
				for (int c = 0; c < taulu[0].length; c++) {
					if (taulu[r][c].equals("S")) {
						System.out.print(" " + "~");
					} else {
						System.out.print(" " + taulu[r][c]);
					}
				}
				System.out.println("");
			}
		}
		valiViiva();
	
	}

	public static void luoAlus(String[][] taulu, int koko) {
		if (Math.random() < 0.5) {
			int col = (int) (Math.random() * 5);
			int row = (int) (Math.random() * 7);
			for (int i = 0; i < koko; i++) {
				taulu[row][col + i] = "S";
			}
		} else {
			int col = (int) (Math.random() * 7);
			int row = (int) (Math.random() * 5);
			for (int i = 0; i < koko; i++) {
				taulu[row + i][col] = "S";
			}
		}
	}

	public static int kayttajaAmpuu(String[][] taulu, int osumat, int torpedot) {
		Scanner input = new Scanner(System.in);
		int row, col;
		System.out.println("Sinulla on: " + torpedot + " torpedoa j‰ljell‰!");
		System.out.println("Valitse rivi, jolle ammut: ");
		row = input.nextInt();
		while (row > 8 || row < 1) // Error checking for row
		{
			System.out.println("Syota validi rivi (1 -> 8)");
			row = input.nextInt();
		}
		System.out.println("Valitse sarake, jolle ammut: ");
		col = input.nextInt();
		while (col > 8 || col < 1) // Error checking for column
		{
			System.out.println("Syota validi sarake (1 -> 8)");
			col = input.nextInt();
		}
		if (taulu[row - 1][col - 1].equals("S")) {
			osumat++;
			System.out.println("~~~~~~~ OSUMA! ~~~~~~~");
			taulu[row - 1][col - 1] = "!";
		} else {
			System.out.println("~~~~~~~ HUTI! ~~~~~~~");
			taulu[row - 1][col - 1] = "M";
		}
		return osumat;
	}

	public static void viimeinen(int osumat, int torpedot) {
		if (osumat < 4)
			System.out.println("Havisit pelin, koska et saanut upotettua laivaa.");
			
		if (torpedot < 1) {
			System.out.println("Menetit kaikki torpedosi");
			System.out.println("Opettele ampumaan!");
		}
		else if (osumat >= 4) {
			System.out.println("Olet voittanut pelin!");
		}
		System.out.println("Peli loppui");
	}

	public static void main(String[] arg){
         String[][] taulu = new String[8][8];
         luoAlusta(taulu);
         luoAlus(taulu, 4);
         int torpedoja = 15;
         int osumia = 0;
         /// Peli alkaa
         while(torpedoja > 0 && osumia < 4)
         {
            naytaTaulu(taulu);
            osumia = kayttajaAmpuu(taulu, osumia, torpedoja);
            torpedoja --;
         }
         viimeinen(osumia, torpedoja);
         naytaTaulu(taulu);
      }
}
